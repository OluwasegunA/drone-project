package com.example.cron_project.entity;

import com.example.cron_project.enums.DroneModel;
import com.example.cron_project.enums.DroneState;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Table(name = "drones")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Drone {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @Size(max = 100)
    private String serialNumber;

    @Enumerated(EnumType.STRING)
    private DroneModel model;

    @DecimalMin(value = "0.10")
    @DecimalMax(value = "500.00")
    @Digits(integer = 3, fraction = 2)
    private BigDecimal weightLimit;

    @Range(min = 1, max = 100)
    private Integer batteryCapacity;

    @Enumerated(EnumType.STRING)
    private DroneState state;

    //uncomment this if Medications are always tied to a drone, and you don't want to use the (DroneMedication) join table
    // but a join column instead
//    @OneToMany(cascade = CascadeType.ALL)
//    @JoinColumn(name = "drone_id", referencedColumnName = "id")
//    private Set<Medication> medications;
}
