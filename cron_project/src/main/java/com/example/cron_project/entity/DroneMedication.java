package com.example.cron_project.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Table(name = "drone_medications")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class DroneMedication {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    private Long droneId;

    private Long medicationId;

    private Integer quantity;
}
