package com.example.cron_project.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;

@Table(name = "medications")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Medication {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @Column(unique = true)
    @NotNull(message="Please enter Medication name")
    @Pattern(regexp = "^([A-Za-z0-9\\-\\_]+)$", message="Medication name must be a character of letters, numbers, hyphen and underscore")
    private String name;

    @Column(unique = true)
    @NotNull(message="Please enter Medication code")
    @Pattern(regexp = "^([A-Z0-9\\-\\_]+)$", message="Medication code must be a character of Upper case letters, numbers, and underscore")
    private String code;

    @JsonProperty(required = true)
    @DecimalMin(value = "0.10", message = "invalid value, ranges from 0.10 to 500.00")
    @DecimalMax(value = "500.00", message = "invalid value, ranges from 0.10 to 500.00")
    @Digits(integer = 3, fraction = 2, message = "invalid value, ranges from 0.10 to 500.00")
    private BigDecimal weight; //using this to allow DP values e.g 0.5g

    @Lob
    private String image; //save image as base-64 String

    //uncomment this if medications are always tied to a drone, and don't need a join table
//    @Column(name = "drone_id")
//    private Long droneId;
}
