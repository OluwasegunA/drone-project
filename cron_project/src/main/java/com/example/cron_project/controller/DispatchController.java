package com.example.cron_project.controller;

import com.example.cron_project.model.BaseResponse;
import com.example.cron_project.model.BaseSuccessResponse;
import com.example.cron_project.model.LoadDroneDto;
import com.example.cron_project.model.RegisterDroneDto;
import com.example.cron_project.service.DroneService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController(value = "api/v1/drones")
@RequiredArgsConstructor
public class DispatchController {

    private final DroneService droneService;

    @Operation(summary = "Register a new drone")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Drone registered successfully",
                    content = { @Content(mediaType = "application/json", schema = @Schema(implementation = BaseSuccessResponse.class))
                    }),
            @ApiResponse(responseCode = "400", description = "Invalid details supplied")
    })
    @PostMapping(value = "", consumes = "application/json")
    public ResponseEntity<BaseResponse> registerDrone(@RequestBody @Valid RegisterDroneDto request){
        var response = droneService.registerDrone(request);
        return ResponseEntity.ok(response);
    }

    @Operation(summary = "Load a new drone")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Drone loaded successfully",
                    content = { @Content(mediaType = "application/json", schema = @Schema(implementation = BaseSuccessResponse.class))
                    }),
            @ApiResponse(responseCode = "400", description = "Invalid details supplied")
    })
    @PostMapping(value = "/load", consumes = "application/json")
    public ResponseEntity<BaseResponse> loadDrone(@RequestBody @Valid LoadDroneDto request){
        var response = droneService.loadDroneWithMedications(request.droneId, request.medicationsId);
        return ResponseEntity.ok(response);
    }

    @Operation(summary = "Check available drones")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Drones available",
                    content = { @Content(mediaType = "application/json", schema = @Schema(implementation = BaseResponse.class))
                    }),
            @ApiResponse(responseCode = "400", description = "Bad Request")
    })
    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<BaseResponse> checkAvailableDrones(){
        var response = droneService.checkAvailableDronesForLoading();
        return ResponseEntity.ok(response);
    }
}
