package com.example.cron_project.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BaseResponse implements IBaseResponse{
    private int statusCode;
    private String description;
    private Object data;

    public BaseResponse(int statusCode, String description) {
    }
}
