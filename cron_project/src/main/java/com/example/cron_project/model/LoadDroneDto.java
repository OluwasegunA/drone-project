package com.example.cron_project.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class LoadDroneDto {

    public Long droneId;

    public List<Long> medicationsId;
}
