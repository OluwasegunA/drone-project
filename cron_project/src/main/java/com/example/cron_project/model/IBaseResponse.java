package com.example.cron_project.model;

public interface IBaseResponse {

    int getStatusCode();
    void setStatusCode(int statusCode);

    String getDescription();
    void setDescription(String description);

}
