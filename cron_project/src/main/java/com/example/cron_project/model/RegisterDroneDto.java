package com.example.cron_project.model;

import com.example.cron_project.enums.DroneModel;
import com.example.cron_project.enums.DroneState;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.*;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class RegisterDroneDto {

    @Size(max = 100)
    @NotBlank
    private String serialNumber;

    @NotNull
    @Enumerated(EnumType.STRING)
    private DroneModel model;

    @JsonProperty(required = true)
    @DecimalMin(value = "0.10", message = "invalid value, ranges from 0.10 to 500.00")
    @DecimalMax(value = "500.00", message = "invalid value, ranges from 0.10 to 500.00")
    @Digits(integer = 3, fraction = 2, message = "invalid value, ranges from 0.10 to 500.00")
    private BigDecimal weightLimit; //using this to allow DP values e.g 0.5g

    @NotNull
    @Range(min = 1, max = 100)
    private Integer batteryCapacity;

    @NotNull
    @Enumerated(EnumType.STRING)
    private DroneState state;

}
