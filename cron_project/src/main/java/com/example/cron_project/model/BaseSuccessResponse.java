package com.example.cron_project.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@NoArgsConstructor
public class BaseSuccessResponse extends BaseResponse {
    @Override
    public int getStatusCode() {
        return HttpStatus.OK.value();
    }
}
