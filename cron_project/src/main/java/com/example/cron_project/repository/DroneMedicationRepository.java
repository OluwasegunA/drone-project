package com.example.cron_project.repository;

import com.example.cron_project.entity.DroneMedication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DroneMedicationRepository extends JpaRepository<DroneMedication, Long> {
    //@Query("SELECT t FROM DroneMedication t WHERE t.droneId = ?1")
    List<DroneMedication> findByDroneId(Long droneId);
}
