package com.example.cron_project.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class ApiException extends UnsupportedOperationException{
    private HttpStatus status;
    private String message;

    public ApiException(String message) {
        this.message = message;
        this.status = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    public ApiException(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }
}
