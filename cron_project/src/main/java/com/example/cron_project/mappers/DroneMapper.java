package com.example.cron_project.mappers;

import com.example.cron_project.entity.Drone;
import com.example.cron_project.model.RegisterDroneDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DroneMapper {

    Drone dtoToEntity(RegisterDroneDto dto);
}
