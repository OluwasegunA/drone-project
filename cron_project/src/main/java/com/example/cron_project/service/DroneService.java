package com.example.cron_project.service;

import com.example.cron_project.entity.Audit;
import com.example.cron_project.entity.Drone;
import com.example.cron_project.entity.DroneMedication;
import com.example.cron_project.entity.Medication;
import com.example.cron_project.enums.DroneState;
import com.example.cron_project.mappers.DroneMapper;
import com.example.cron_project.model.BaseResponse;
import com.example.cron_project.model.BaseSuccessResponse;
import com.example.cron_project.model.RegisterDroneDto;
import com.example.cron_project.repository.AuditRepository;
import com.example.cron_project.repository.DroneMedicationRepository;
import com.example.cron_project.repository.DroneRepository;
import com.example.cron_project.repository.MedicationRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.description.method.ParameterList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
@EnableScheduling //needed for scheduling the cron job
@RequiredArgsConstructor
public class DroneService {

    private final DroneRepository droneRepository;
    private final DroneMapper droneMapper;
    private final MedicationRepository medicationRepository;
    private final AuditRepository auditRepository;
    private final DroneMedicationRepository droneMedicationRepository;

    public BaseResponse registerDrone(RegisterDroneDto dto){

        //use mapper to convert from dto to entity
        var drone = droneMapper.dtoToEntity(dto);

        droneRepository.save(drone);

        var response = new BaseSuccessResponse();
        response.setDescription("drone registered successfully");

        return response;
    }

    public BaseResponse loadDroneWithMedications(long droneId, List<Long> medicationIds){
        BaseResponse response = new BaseResponse();

        try {
            var loadingDroneObject = droneRepository.getById(droneId);
            var weightCounter = 0;
            if(checkAvaliableDrones().contains(loadingDroneObject)) {
                for(long medicationId : medicationIds){

                    if(medicationRepository.existsById(medicationId)){
                        var medication = medicationRepository.getById(medicationId);
                        weightCounter += medication.getWeight().intValue();
                        while(weightCounter < loadingDroneObject.getWeightLimit().intValue()){
                            var droneMedication = new DroneMedication();
                            droneMedication.setDroneId(loadingDroneObject.getId());
                            droneMedication.setMedicationId(medication.getId());
                            droneMedicationRepository.save(droneMedication);
                        }
                    }
                }

                response.setDescription("Drone loaded Successfully");
                response.setStatusCode(HttpServletResponse.SC_OK);
            }
            else{
                response.setDescription("Drone not Found");
                response.setStatusCode(HttpServletResponse.SC_NOT_FOUND);
            }

        } catch (IllegalArgumentException ex) {
            System.out.println(ex.getMessage());
            response.setDescription("Drone not loaded");
            response.setStatusCode(HttpServletResponse.SC_BAD_REQUEST);
            log.error(response.getDescription());
        }
        return response;
    }

    public BaseResponse checkLoadedMedication(long droneId){
        BaseResponse response = new BaseResponse();

        try{
            List<Medication> medications = new ArrayList<>();
            if(droneRepository.existsById(droneId)){
                var droneMedications = droneMedicationRepository.findByDroneId(droneId);
                for (DroneMedication droneMedication: droneMedications
                     ) {
                    var medication = medicationRepository.getReferenceById(droneMedication.getMedicationId());
                    medications.add(medication);
                }
                if(!medications.isEmpty()){
                    response.setData(medications);
                    response.setDescription("Drone is loaded with medication items");
                    response.setStatusCode(HttpServletResponse.SC_OK);
                    log.info(response.getDescription() + ": {}", medications.toString());
                }
                else{
                    response.setDescription("No medication items loaded for Drone.");
                    response.setStatusCode(HttpServletResponse.SC_OK);
                    log.info(response.getDescription());
                }
            }
            else{
                response.setDescription(String.format("No Drone with the Id %2d found.", droneId));
                response.setStatusCode(HttpServletResponse.SC_NOT_FOUND);
                log.error(response.getDescription());
            }
        }catch (IllegalArgumentException ex) {
            System.out.println(ex.getMessage());
            response.setDescription(ex.getMessage());
            response.setStatusCode(HttpServletResponse.SC_BAD_REQUEST);
            log.error(response.getDescription());
        }
        return response;
    }

    public BaseResponse checkAvailableDronesForLoading(){
        var response = new BaseResponse();
        try{
            var availableDrones = checkAvaliableDrones();

            response.setData(availableDrones);
            response.setStatusCode(HttpServletResponse.SC_OK);
            response.setDescription("Successful");
        }catch (IllegalArgumentException ex) {
            System.out.println(ex.getMessage());
            response.setDescription(ex.getMessage());
            response.setStatusCode(HttpServletResponse.SC_BAD_REQUEST);
            log.error(response.getDescription());
        }
        return response;
    }

    public ArrayList<Drone> checkAvaliableDrones(){
        var availableDrones = new ArrayList<Drone>();

        try{
            var droneList = droneRepository.findAll();
            for (Drone drone: droneList
                 ) {
                if((drone.getState() == DroneState.IDLE || drone.getState() == DroneState.LOADING)
                        && drone.getBatteryCapacity() >= 25)
                {
                    availableDrones.add(drone);
                }
            }
            return availableDrones;
        }catch (IllegalArgumentException ex) {
            System.out.println(ex.getMessage());
            log.error(ex.getMessage());
            return null;
        }
    }

    @Scheduled(cron = "0 0 12 * * *") //scheduled for 12pm every day
    void checkDronesBatteryLevel(){
        var droneList = droneRepository.findAll();

        for (Drone drone: droneList
             ) {
            if(drone.getBatteryCapacity() < 25){
                //drone.setState(DroneState.LOW);
                //Drone updatedDrone = droneRepository.save(drone);

                Audit audit = new Audit();
                audit.setDroneId(drone.getId());
                audit.setBatteryLevel(drone.getBatteryCapacity());
                auditRepository.save(audit);
            }
        }
    }
}
