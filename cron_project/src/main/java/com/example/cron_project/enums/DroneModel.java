package com.example.cron_project.enums;

public enum DroneModel {
    LIGHTWEIGHT("Lightweight"),
    MIDDLEWEIGHT("Middleweight"),
    CRUISERWEIGHT("Cruiserweight"),
    HEAVYWEIGHT("Heavyweight");

    public String getName() {
        return name;
    }

    private final String name;

    DroneModel(String name) {
        this.name = name;
    }
}
