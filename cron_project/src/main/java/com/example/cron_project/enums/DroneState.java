package com.example.cron_project.enums;

public enum DroneState {
    IDLE("Idle"),
    LOADING("Loading"),
    LOADED("Loaded"),
    DELIVERING("Delivering"),
    DELIVERED("Delivered"),
    RETURNING("Returning");

    public String getName() {
        return name;
    }

    private final String name;

    DroneState(String name) {
        this.name = name;
    }
}
